# LogoViz
# How to install
1. Download Python 2.7.13 and set up required Environment variables.
2. Install all C++ binaries using “npm install --global windows-build-tools” 
3. Download OpenCV for Python and set up required environment variables.
    - OPENCV_DIR points to C:\OpenCV\build\x64\vc14
    - add to path %OPENCV_DIR%\bin
4. Clone the repo
5. Ensure Node and NPM is installed then run "npm install" to install dependencies
6. download YouTube-DL for Python
    - Replace the YouTube-DL.exe from node module with the Python YouTube-DL.exe
7. Ensure a MySQL database is running on localhost: go to datastore.js from config folder to change username/password if required.
8. Run command Sails Lift
