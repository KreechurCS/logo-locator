let skipper = require("skipper")
var filePath;
 module.exports = async function newbrand(req, res){
  if(req.method === 'GET'){
    return res.json({'status' : 'GET is not allowed'
  });
  }

  var test = req.param("brand");
  var web = req.param("website");
  console.log(test);
  var uploadFile = req.file('uploadFile');
  await uploadFile.upload({
    dirname: require('path').resolve(sails.config.appPath, 'assets/logos')},
    async function onUploadCompleter(err, files) {
    if(err) {
      return res.serverError(err);
    }
    console.log(files);
    console.log(files[0].filename);
    console.log(files[0].fd);
    filePath = files[0].fd;
    filePath = filePath.split('\\');
    console.log(filePath);

      //This is where the entry in the database for the brand is being created
      //It will pass the name of the newly uploaded logo into logoPath as the
      //route will always be /assets/brands/logoPath
      await Brands.create({
        brandName: test,
        brandWebsite: web,
        logoPath: filePath[8],
        userCreated: "Debug",
      })
    return res.redirect('/');
  });
};

 