module.exports =  async function displayQuiz (req,res) {

    const cv = require('opencv4nodejs');
    //Include Template matching as well
    //Add ratio test to remove erroneous matches

    const img1 = cv.imread('./DIT2.png');
    const img2 = cv.imread('./test.png');
    const detector = new cv.SURFDetector({hessianThreshold: 400});

    const keypoint1 = detector.detect(img1);
    const keypoint2 = detector.detect(img2);

    const descriptors1 = detector.compute(img1, keypoint1);
    const descriptors2 = detector.compute(img2, keypoint2);

    const matches = new cv.matchKnnFlannBased(descriptors1, descriptors2, 2);

    var bestMatches = [];
    matches.forEach(match => {
      if (match[0].distance < 0.8 * match[1].distance) {
        if (match[0].distance < 0.15) {
          bestMatches.push(match[0]);
        }
      }
    });
    bestMatches = bestMatches.sort(
      (match1, match2) => match1.distance - match2.distance);

    var indexs = checkDensity({
      img: img2,
      matches: bestMatches,
      keypoint1: keypoint1,
      keypoint2: keypoint2
    });

    var cleaned = removeBad({
      matches: bestMatches,
      indexs: indexs
    });
    
    if(bestMatches.length > 15){
      drawArea({
        img: img2,
        bestMatches: cleaned,
        keyPoints2: keypoint2,
        color: new cv.Vec3(0, 255, 0)
      });
    }

    const drawn = cv.drawMatches(img1, img2, keypoint1, keypoint2, cleaned);
    drawn.putText("Keypoints Matched: " + bestMatches.length, new cv.Point2(10, 20), 1, 2, new cv.Vec(255, 255, 0), 2, 0);

    cv.imshowWait("Test", drawn);
    cv.destroyAllWindows();
    return res.view('pages/processing/keypoint', {numMatches: [1,2], percentages: [1,2], onscreen: [1,2]});
};


const checkDensity = ({img, matches, keypoint1, keypoint2}) => {
  const cv = require('opencv4nodejs');
  var points = [];
  var badIndexs = [];
  var averageX = 0;
  var averageY = 0;
  var totalDist = 0;
  var averageDist = 0;

  console.log(matches);
  matches.forEach(match => {
    points.push({index: match.trainIdx,
      x:[keypoint2[match.trainIdx].point.x],
      y:[keypoint2[match.trainIdx].point.y]
      });
  });
  points.push({index: 999999,
  x:700,
y:800});
  console.log(points);

  //Calculate average point
  for(var i = 0; i < points.length; i++){
    averageX = averageX + parseInt(points[i].x);
    averageY = averageY + parseInt(points[i].y);
  }
      
  averageX = averageX/points.length;
  averageY = averageY/points.length;
  console.log(averageX);
  var average = new cv.Point2(averageX, averageY);
  img.drawCircle(average, 10, new cv.Vec(255, 0, 0), 5);
  img.drawCircle(new cv.Point2(700,800), 10, new cv.Vec(255, 0, 0), 5);

  //calculate average distance to average point
  
  points.forEach(point => {
    var point = new cv.Point2(point.x,point.y);
    var distance = Math.abs(point.x - average.x) + Math.abs(point.y - average.y);
    totalDist += distance;
  });
  averageDist = totalDist/points.length;

  //Check if point is within bounds, if not remove index from match
  points.forEach(pointi => {
    var point = new cv.Point2(pointi.x,pointi.y);
    var distance = Math.abs(point.x - average.x) + Math.abs(point.y - average.y);
    if(distance < 1.5*averageDist){
      img.drawLine(point, average, new cv.Vec(255,255,255), 1);
    }
    else{
      badIndexs.push(pointi);
    }
  });
  console.log("=================================");
  console.log(badIndexs);
  return badIndexs;
}

const removeBad = ({
  matches,
  indexs
}) => {
  var test = [];
  var cleanMatch = [];
  var lock = false;
  console.log("================");
  matches.forEach(match => {
    test.push(match);
    lock = false;
    indexs.forEach(index => {
      console.log(match.trainIdx + "   " + index.index);
      console.log(test.length);
      if(match.trainIdx == index.index && lock == false){
        test.pop();
        lock = true;
      }
    });
  });
  console.log("=======================");
  console.log(test);
  return test;
}

const drawArea = ({
  img,
  bestMatches,
  keyPoints2,
  color
}) => {
  const cv = require('opencv4nodejs');
  var test1 = [];
  var test2 = [];
  for (var i = 0; i < bestMatches.length; i++) {
    test2.push(keyPoints2[bestMatches[i].trainIdx].point);
  }

  var cornery = test2[0].y;
  var cornerx = test2[0].x;
  var cornery2 = test2[0].y;
  var cornerx2 = test2[0].x;

  for (var i = 1; i < test2.length; i++) {
    if (test2[i].y < cornery) {
      cornery = test2[i].y;
    }
    if (test2[i].x < cornerx) {
      cornerx = test2[i].x;
    }
    if (test2[i].y > cornery2) {
      cornery2 = test2[i].y;
    }
    if (test2[i].x > cornerx2) {
      cornerx2 = test2[i].x;
    }
  }

  test2 = test2.sort();
  var point = new cv.Point2(cornerx, cornery);
  var far = new cv.Point2(cornerx2, cornery2);
  img.drawCircle(point, 5, new cv.Vec(0, 0, 255), 5);
  img.drawCircle(far, 5, new cv.Vec(0, 0, 255), 5);
  var width = far.x - point.x;
  var height = far.y - point.y;
  var box = new cv.Rect(cornerx, cornery, width, height);
  img.drawRectangle(box, color, 2);
}