module.exports = {


    friendlyName: 'Videodl',
  
  
    description: 'Videodl something.',
  
  
    inputs: {
      videoURL: {
        type: "string",
        required: true,
      },
  
      Logo: {
        type: "string",
        required: true
      }
    },
  
  
    exits: {
      success: {
        responseType: 'redirect',
      }
    },
  
  
    fn: async function (inputs, exits) {
  
      console.log("Video URL    " + inputs.videoURL);
      console.log("Brand Name  " + inputs.brandName);
      return exits.success('/videoKey?videoURL='+inputs.videoURL+'&logo='+inputs.Logo);
  
    }
  
  
  };
  