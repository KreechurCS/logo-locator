module.exports =  async function displayQuiz (req,res) {

    const cv = require('opencv');
    var COLOR = [255,255,255];
    var threshhold = 0.8;
    var matrux;

    cv.readImage('./sample-nike.png', function (err, img) {
        img.convertGrayscale(); //Converts rgb image to greyscale
        img.threshold(100, 255, "Binary"); //coverts grayscale image to binary image
        img.gaussianBlur([3,3]); //reduces the size of the image by 3 then re expands to reduce noise and smooth edges
        img.canny(0,150); //Uses canny edge detection to outline the edges of the image
        img.dilate(2); //Expands the images edges by 2
        // var contours = img.findContours();
        // console.log(contours);
        // var allContoursImg = img.drawAllContours(contours, COLOR);
        // allContoursImg.save("contour.jpg");
        img.save("thresh.jpg");
        console.log(img);
        matrux = img;
    });


    console.log(matrux);

    cv.readImage('./test.png', function (err, im) {
        if (err) throw err;
        if (im.width() < 1 || im.height() < 1) throw new Error('Image has no size');
        console.log(im);
        im.convertGrayscale();
        im.threshold(100, 255, "Binary");
        im.gaussianBlur([3,3]);
        im.canny(0,150);
        im.dilate(2);

        for (var i = 0; i <=5; i++)
        {
            var image = im.copy();
            var output = image.matchTemplate("thresh.jpg", i);
        // var output = im.matchTemplateByMatrix(matrux, 1);
        // var matches = output.templateMatches(0.80, 1.0, 1, false);
        // console.log(output);
        // console.log(matches);
        // matches.forEach(val => {
        //     console.log(val.x);
        //     console.log(val.y);
        //     im.rectangle([val.x, val.y], [val.x + 100, val.y + 50], COLOR, 2);
        // });
        image.rectangle([output[1], output[2]], [output[3], output[4]], COLOR, 2);
        var filename = "./assets/images/out" + i + ".jpg";
        image.save(filename);
        }
        
    });
     
    
    return res.view('pages/processing/openCV', {});
    //{quiz: retrievedQuiz, count: timesTaken} values to pass
};
