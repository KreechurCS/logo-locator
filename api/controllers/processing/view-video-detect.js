var heatPointX = [];
var heatPointY = [];
var heatSize = [];

module.exports = async function videoDetect (req, res) {

const cv = require('opencv4nodejs');
var video = './myvideo.mp4';

const detector = new cv.SURFDetector({
});

console.log(req.param("videoURL"));
const delay = 1;
const frameSkip = 30;
var frameCount = 0;
var percentages = [];
var numMatches = [];

var onscreen = 0;
const logoPath = "assets/logos/"+req.param("logo");
console.log(logoPath);
const logo = cv.imread(logoPath);
const keyPoints1 = detector.detect(logo);
const descriptors1 = detector.compute(logo, keyPoints1);
//const out = new cv.VideoWriter("./out.mp4",828601953,30, new cv.Size(1920, 1080));


grabFrames(video, delay, (frame) => {
  var temp = logo.cvtColor(cv.COLOR_RGB2GRAY);
  if(frameCount % frameSkip == 0 && !frame.empty){
    frame = frame.cvtColor(cv.COLOR_RGB2GRAY);
    var keyPoints2 = detector.detect(frame);
    var matches = [];
    console.log(keyPoints2.length); //TODO Implement check to see if keypoints2 is greater than 0
    if(keyPoints2.length > 0){

      var descriptors2 = detector.compute(frame, keyPoints2);

      matches = new cv.matchKnnBruteForce(descriptors1, descriptors2, 2);

    }
    
    var bestMatches = [];
    matches.forEach(match => {
      if (match[0].distance < 0.8 * match[1].distance) {
        if (match[0].distance < 0.1) {
          bestMatches.push(match[0]);
        }
      }
    });
    bestMatches = bestMatches.sort(
      (match1, match2) => match1.distance - match2.distance);

    var indexs = checkDensity({
      img: frame,
      matches: bestMatches,
      keyPoints1: keyPoints1,
      keyPoints2: keyPoints2
    });

    var cleaned = removeBad({
      matches: bestMatches,
      indexs: indexs
    });
    
    if(bestMatches.length > 15){
      var percent = drawArea({
        img: frame,
        bestMatches: cleaned,
        keyPoints2: keyPoints2,
        color: new cv.Vec3(0, 255, 0)
      });
      percentages.push(percent);
      onscreen++;
    }
    else{ percentages.push(0);}
    
    //out.write(test);
    const test = cv.drawMatches(temp, frame, keyPoints1, keyPoints2, cleaned);

    test.putText("Keypoints Matched: " + cleaned.length, new cv.Point2(10, 20), 1, 2, new cv.Vec(255, 255, 0), 2, 0);
    cv.imshow('frame', test);
    numMatches.push((cleaned.length/keyPoints1.length) * 100);
    if(frameCount == 717){
      cv.destroyAllWindows();
    }
  }
  if(frame.empty){
    // console.log(heatSize);
    // console.log(heatPointX);
    // console.log(heatPointY);
    return res.view('pages/processing/keypoint', {numMatches: numMatches, percentages: percentages, onscreen: onscreen, heatPointX: heatPointX, heatPointY: heatPointY, heatSize: heatSize});
  }
  ++frameCount;
});
};
//out.release();

const checkDensity = ({img, matches, keyPoints1, keyPoints2}) => {
  const cv = require('opencv4nodejs');
  var points = [];
  var badIndexs = [];
  var averageX = 0;
  var averageY = 0;
  var totalDist = 0;
  var averageDist = 0;

  matches.forEach(match => {
    points.push({index: match.trainIdx,
      x:[keyPoints2[match.trainIdx].point.x],
      y:[keyPoints2[match.trainIdx].point.y]
      });
  });
  points.push({index: 999999,
  x:700,
y:800});

  //Calculate average point
  for(var i = 0; i < points.length; i++){
    averageX = averageX + parseInt(points[i].x);
    averageY = averageY + parseInt(points[i].y);
  }
      
  averageX = averageX/points.length;
  averageY = averageY/points.length;
  
  var average = new cv.Point2(averageX, averageY);

  //calculate average distance to average point
  
  points.forEach(point => {
    var point = new cv.Point2(point.x,point.y);
    var distance = Math.abs(point.x - average.x) + Math.abs(point.y - average.y);
    totalDist += distance;
  });
  averageDist = totalDist/points.length;

  //Check if point is within bounds, if not remove index from match
  points.forEach(pointi => {
    var point = new cv.Point2(pointi.x,pointi.y);
    var distance = Math.abs(point.x - average.x) + Math.abs(point.y - average.y);
    if(distance > 1.5*averageDist){
      badIndexs.push(pointi);
    }
  });
  return badIndexs;
}

const removeBad = ({
  matches,
  indexs
}) => {
  var test = [];
  var cleanMatch = [];
  var lock = false;
  matches.forEach(match => {
    test.push(match);
    lock = false;
    indexs.forEach(index => {
      if(match.trainIdx == index.index && lock == false){
        test.pop();
        lock = true;
      }
    });
  });
  return test;
}

const drawArea = ({
  img,
  bestMatches,
  keyPoints2,
  color
}) => {
  const cv = require('opencv4nodejs');
  var test1 = [];
  var test2 = [];
  for (var i = 0; i < bestMatches.length; i++) {
    test2.push(keyPoints2[bestMatches[i].trainIdx].point);
  }

  var cornery = test2[0].y;
  var cornerx = test2[0].x;
  var cornery2 = test2[0].y;
  var cornerx2 = test2[0].x;

  for (var i = 1; i < test2.length; i++) {
    if (test2[i].y < cornery) {
      cornery = test2[i].y;
    }
    if (test2[i].x < cornerx) {
      cornerx = test2[i].x;
    }
    if (test2[i].y > cornery2) {
      cornery2 = test2[i].y;
    }
    if (test2[i].x > cornerx2) {
      cornerx2 = test2[i].x;
    }
  }

  test2 = test2.sort();
  var point = new cv.Point2(cornerx, cornery);
  var far = new cv.Point2(cornerx2, cornery2);
  var width = far.x - point.x;
  var height = far.y - point.y;
  var box = new cv.Rect(cornerx, cornery, width, height);
  img.drawRectangle(box, color, 2);
  var percent = getLogoPercentage({
    width: width,
    height: height
  });
  var temp1 = cornerx + width/2;
  var temp2 = cornery + height/2;
  var test = (percent/100) * (width/2);
  console.log(temp1 + "   " + temp2);
  heatSize.push(test);
  heatPointX.push(temp1);
  heatPointY.push(temp2);
  return percent;
}

const getLogoPercentage = (width) => {
  const cv = require('opencv4nodejs');
  const LogoSize = width;
  const area = LogoSize.width * LogoSize.height;
  const videoArea = 1200 * 800;
  console.log(area);

  const percentage = (area/videoArea) * 100;
  return percentage;

}

const grabFrames = (videoFile, delay, onFrame) => {
  const cv = require('opencv4nodejs');

  const cap = new cv.VideoCapture(videoFile);
  const reso = cap.get(cv.CAP_PROP_FPS);
  const widt = cap.get(cv.CAP_PROP_FRAME_WIDTH);
  const heit = cap.get(cv.CAP_PROP_FRAME_HEIGHT);
  const length = cap.get(cv.CAP_PROP_FRAME_COUNT);
  console.log("lenght: "  + length);
  console.log("FPS: " + reso);
  console.log("Width and Height: " + widt + "  " + heit);
  let done = false;
  const intvl = setInterval(() => {
    let frame = cap.read();
    // loop back to start on end of stream reached
    if (frame.empty) {
      //cap.reset();
      done = true;
      clearInterval(intvl);
      cap.release();
      console.log('Key pressed, exiting.');
      cv.destroyAllWindows();
      //frame = cap.read();
    }
    onFrame(frame);

    const key = cv.waitKey(delay);
    done = key !== -1 && key !== 255;
    if (done) {
      clearInterval(intvl);
      cap.release();
      console.log('Key pressed, exiting.');
      
      cv.destroyAllWindows();

    }
  }, 0);
};
