module.exports = {


  friendlyName: 'Videodl',


  description: 'Videodl something.',


  inputs: {
    videoURL: {
      type: "string",
      required: true,
    },

    brandName: {
      type: "string",
      required: true
    }
  },


  exits: {
    success: {
      responseType: 'redirect',
    }
  },


  fn: async function (inputs, exits) {

    console.log("Video URL    " + inputs.videoURL);
    console.log("Brand Name  " + inputs.brandName);
    return exits.success('/video?videoURL='+inputs.videoURL+'&brandName='+inputs.brandName);

  }


};
