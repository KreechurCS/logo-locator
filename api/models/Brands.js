/**
 * Brands.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    brandName: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 100,
      example: 'LogoViz',
      description: 'the name of the brand'
    },

    brandWebsite: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 100,
      example: 'LogoViz',
      description: 'the name of the brand'
    },

    logoPath: {
      type: 'string',
      required: true,
      maxLength: 100,
      example: '/images/logoViz.png',
      description: 'The path of the brand logo that will be used as the brand template'
    },

    userCreated: {
      type: 'string',
      required: true,
      description: 'Users full name',
      maxLength: 120,
      example: 'Steve Martin'
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝
    // n/a

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    // n/a
  },

};

